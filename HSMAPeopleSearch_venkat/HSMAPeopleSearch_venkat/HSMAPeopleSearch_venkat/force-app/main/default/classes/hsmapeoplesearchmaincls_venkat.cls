public with sharing class hsmapeoplesearchmaincls_venkat {
  public hsmapeoplesearchmaincls_venkat() {

  }
  //below method will be used in mainComponent 
  @AuraEnabled(Cacheable = true)
  public static List<contact> retriveContacts(String strContactName) 
  {
  List<contact> lstcont=new List<contact>();
  String query = 'SELECT  Id,Account_Name_Formula__c,AccountId,Account.Name,Name, Email,HSMA_Address__c,Phone,Birthdate,Age__c,MobilePhone,MailingAddress,MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet FROM contact order by lastmodifieddate desc';
  if(strContactName != null && strContactName != '') 
  {      
      query = 'FIND \'' + strContactName + '\' IN ALL FIELDS RETURNING  contact (id,accountid,Account_Name_Formula__c,Email,Name,Phone,Birthdate,Age__c,MobilePhone,MailingCity,MailingState,MailingCountry,MailingPostalCode,MailingStreet)';
      List<List <sObject>> searchList = search.query(query);
      lstcont = ((List<contact>)searchList[0]);  
  }
  else {
      lstcont = Database.query(query);  
  }   
  return lstcont;
  }
  @AuraEnabled(cacheable = true) 
  public static List<Opportunity> getOpportunitiesList(string conid) {       
    list<opportunity> OpportunityList = new list<opportunity> ();      
    OpportunityList = [select id, name,StageName,Type,CloseDate from Opportunity where contactid=:conid order by lastmodifieddate desc];
    system.debug('>>>>>>>>>>>>>>> conAccOppsList : ' + OpportunityList.size() );
    if(OpportunityList.size() == 0){
        throw new AuraHandledException('No Record Found'); 
    }
    return OpportunityList;
   }
   @AuraEnabled(cacheable = true) 
   public static List<contact> getConlist(string conid) {       
    list<contact> conlist = new list<contact> ();      
    conlist = [select id, name,email,phone from contact where id=:conid order by lastmodifieddate desc];
    system.debug('>>>>>>>>>>>>>>> conAccOppsList : ' + conlist.size() );
    if(conlist.size() == 0){
        throw new AuraHandledException('No Details Found'); 
    }
    return conlist;
   }
   @AuraEnabled(cacheable = true) 
  public static List<Task> getTasksList(string oppid) {       
    list<task> TaskList = new list<task> ();      
    TaskList = [select id, Subject_URL__c,subject,Subject_Formulae__c,Comments__c,Priority,Status,Type,OwnerId,Owner.Name,ActivityDate from task where whatid=:oppid order by lastmodifieddate desc];
    system.debug('>>>>>>>>>>>>>>> TaskList : ' + TaskList.size() );
    if(TaskList.size() == 0){
        throw new AuraHandledException('No Record Found'); 
    }
    
    return TaskList;
   } 
   @AuraEnabled(cacheable = true) 
  public static List<Task> getTasksList1(string oppid, String aOrder, string aField ) {       
    //list<task> TaskList = new list<task> ();      
    string TaskQuery = '';
    
    if(aorder != '') {
      TaskQuery = 'select id,Subject_URL__c, Subject_Formulae__c,subject,Comments__c,Priority,Status,Type,OwnerId,Owner.Name,ActivityDate from task where whatid=:oppid order by ' + aField  + '  ' + aOrder;      
    } else {
    
     TaskQuery = 'select id,Subject_URL__c, Subject_Formulae__c,subject,Comments__c,Priority,Status,Type,OwnerId,Owner.Name,ActivityDate from task where whatid=:oppid order by lastmodifieddate desc';
    }

system.debug('>>>>>>>>>>>>>>> TaskQuery : ' + TaskQuery  );
system.debug('>>>>>>>>>>>>>>> aOrder: ' + aOrder );
system.debug('>>>>>>>>>>>>>>> aField : ' + aField );

//      TaskQuery = 'select id, subject,Comments__c,Priority,Status,Type,OwnerId,Owner.Name,ActivityDate from task where whatid=:oppid order by lastmodifieddate desc';

     List<task> TaskList = Database.query(TaskQuery);
     
    return TaskList;
   } 

    @AuraEnabled(cacheable = true) 
    public static List<Task> getTasksList(Integer pagenumber, Integer numberOfRecords, Integer pageSize, String searchString) {  
      String searchKey = '%' + searchString + '%';  
      String query = 'select id,Subject_URL__c, Subject_Formulae__c,Name,Comments__c,Priority from Task order by lastmodifieddate desc';  
       
      return Database.query(query);  
    }  
  @AuraEnabled(cacheable = true)  
  public static Integer getAccountsCount(String searchString) {  
    String query = 'select count() from Account order by lastmodifieddate desc';  
    system.debug('query****'+query);
    return Database.countQuery(query);  
  } 
  @AuraEnabled(cacheable = true)  
  public static Integer getContactsCount(String searchString) {  
    list<contact> ContactsSearchList = new list<contact>();
    Integer concount=0;
    String searchStr = '*'+searchString+'*';
    String searchQuery = 'FIND \'' + searchStr + '\' IN ALL FIELDS RETURNING  contact (id,accountid,Email,Name,Phone)';
    List<List <sObject>> searchList = search.query(searchQuery);
    ContactsSearchList = ((List<contact>)searchList[0]);

    system.debug('Contact size: ' + ContactsSearchList.size() );


    if(ContactsSearchList.size() == 0){
        throw new AuraHandledException('No Record Found'); 
    }
    else {
      {
        concount=ContactsSearchList.size();
      }
    }
    return concount;
  } 
  @AuraEnabled(cacheable=true)
  public static list<ActivityHistory> getTasksHistoryList(string oppid) {
      system.debug('ConOppID in getTaskHistoryList: ' + oppid);
      list<ActivityHistory> oppTaskHist = new list<ActivityHistory>();
      Opportunity selectedConOpp = new Opportunity();
      selectedConOpp = [SELECT Id, (SELECT Id, Subject,Priority, Status, Owner.Name FROM ActivityHistories WHERE IsTask = TRUE) FROM Opportunity WHERE id =: oppid];
      oppTaskHist = selectedConOpp.ActivityHistories;
      system.debug('******* taskHistoryList Size: ' + oppTaskHist.size() );
      if(oppTaskHist.size() == 0){
          throw new AuraHandledException('No Record Found'); 
      }
      return oppTaskHist;
  } 
  @AuraEnabled(cacheable=true)
  public static List<Campaign> fetchCampaigns(string accId) {
      //Contact c = [select Id from Contact where accountId = : accId limit 1];
      Contact c = [select Id from Contact where id = : accId limit 1];
      set<id> camid=new set<id>();
      List<Campaign> camlist=new List<Campaign>();
      List<CampaignMember> camlst=[select Id,Campaignid,Campaign.Name,Campaign.Status,Campaign.Type,Campaign.CreatedBy.Name,Campaign.CreatedDate from CampaignMember
                                      where contactId = :c.Id order by lastmodifieddate desc];
      if(camlst.size()>0)
      {
          for(CampaignMember camlst1:camlst)
          {
            camid.add(camlst1.Campaignid);
          }
      }
      if(camid.size()>0)
      {
        camlist=[select Id,Name,SMAExternalReferenceURL__c,Status,Type,CreatedBy.Name,CreatedDate from Campaign where id=:camid];
      } 
      return camlist;                               
  } 
  @AuraEnabled(cacheable=true)
  public static List<CampaignMember> fetchCampaignmembers(string accId) {
      //Contact c = [select Id from Contact where accountId = : accId limit 1];
      Contact c = [select Id from Contact where id = : accId limit 1];
      set<id> camid=new set<id>();
      List<Campaign> camlist=new List<Campaign>();
      List<CampaignMember> camlst=[select Id,Campaignid,Campaign.Name,Campaign.Status,Campaign.Type,Campaign.CreatedBy.Name,Campaign.CreatedDate from CampaignMember
                                      where contactId = :c.Id order by lastmodifieddate desc];      
      return camlst;                               
  }
  @AuraEnabled
  public static task saveTask(string subject,string description,string priority,string status,string relatedto)
  {
    system.debug('inside task');
    system.debug('inside task'+relatedto);
    task t = new task();
    t.WhatId=relatedto;
    t.status=status;
    t.subject = subject;
    t.description = description;
    t.Comments__c = description;
    t.priority = priority;
    insert t;
    system.debug('inside task'+t);
    return t;
  }
}