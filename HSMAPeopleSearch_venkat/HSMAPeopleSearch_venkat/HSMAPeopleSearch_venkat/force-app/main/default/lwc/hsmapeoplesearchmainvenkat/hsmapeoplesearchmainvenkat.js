import { LightningElement, track, wire ,api} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import serachCons from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.retriveContacts';
import getOpportunitiesList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getOpportunitiesList';
import getTasksList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getTasksList';
import FirstNAME_FIELD from '@salesforce/schema/contact.FirstName';
import LastNAME_FIELD from '@salesforce/schema/contact.LastName';
import TITLE_FIELD from '@salesforce/schema/contact.Title';
import PHONE_FIELD from '@salesforce/schema/contact.Phone';
import MOBILE_FIELD from '@salesforce/schema/contact.Phone';
import EMAIL_FIELD from '@salesforce/schema/contact.Email';
import CONTACTTYPE_FIELD from '@salesforce/schema/contact.zipsma__ContactType__c';
import MARKETSEGMENT_FIELD from '@salesforce/schema/contact.zipsma__MarketSegment__c';
import OWNER_FIELD from '@salesforce/schema/contact.OwnerId';
import DOB_FIELD from '@salesforce/schema/contact.Birthdate';
import Age_FIELD from '@salesforce/schema/contact.Age__c';
import Mobile_FIELD from '@salesforce/schema/contact.MobilePhone';
import City_FIELD from '@salesforce/schema/contact.MailingCity';
import State_FIELD from '@salesforce/schema/contact.MailingState';
import Country_FIELD from '@salesforce/schema/contact.MailingCountry';
import Postal_FIELD from '@salesforce/schema/contact.MailingPostalCode';
import datatablelwc from '@salesforce/resourceUrl/datatablelwc'
import { loadStyle } from 'lightning/platformResourceLoader';

export default class Hsmapeoplesearchmainvenkat extends LightningElement {
    @track data;
    @track strSearchContactName = '';
    result;
//this is initialize for 1st page
    @track page = 1;
//it contains all the Product records.
    @track items = [];
//To display the data into datatable
    @track data = [];
//holds column info.
    @track columns;
    @track sortBy;
    @track sortDirection;
//start record position per page
    @track startingRecord = 1;
//end record position per page
    @track endingRecord = 0;
//10 records display per page
    @track pageSize = 5;
//total count of record received from all retrieved records
    @track totalRecountCount = 0;
//total number of page is needed to display all records
    @track totalPage = 0;
    @track maxrowselection = 1;
//To display the column into the data table
   @track accounts;
   @track contacts;
   @track contacts1;
   @track opportunities;
   @track opportunities1;
   @track tasks;
   @track error;
   @track error1;
   @track error2;
   @track searchKey;
   @track detailid;
   @track cardtitle;
   @track relobjname;
   @track selectedconId;  
   @track RecDetailFields = [FirstNAME_FIELD,LastNAME_FIELD, TITLE_FIELD, PHONE_FIELD, MOBILE_FIELD, EMAIL_FIELD,CONTACTTYPE_FIELD,MARKETSEGMENT_FIELD,OWNER_FIELD,DOB_FIELD,Age_FIELD,Mobile_FIELD,City_FIELD,State_FIELD,Country_FIELD,Postal_FIELD,OWNER_FIELD];
   @track columns = [
    {
     label: 'Name',
     fieldName: 'Name',
     type: 'text',
     sortable: "true"
     },
     {
        label: 'DOB',
        fieldName: 'Birthdate',
        type: 'date',
        sortable: "true"
    },
    {
        label: 'Age',
        fieldName: 'Age__c',
        type: 'text',
        sortable: "true"
    },
    {
        label: 'Phone',
        fieldName: 'Phone',
        type: 'phone',
        sortable: "true"
    },
    {
        label: 'Mobile',
        fieldName: 'MobilePhone',
        type: 'phone',
        sortable: "true"
    },
    {
        label: 'Email',
        fieldName: 'Email',
        type: 'email',
        sortable: "true"
    },
    
    ];
    
    //call the apex method and pass the search string into apex method.
    @wire(serachCons, {strContactName : '$strSearchContactName' })
    wiredProducts({ error, data }) {
        if (data) {
            this.items = data;
            this.totalRecountCount = data.length;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
            this.data = this.items.slice(0,this.pageSize);
            this.endingRecord = this.pageSize;
            this.error = undefined;
        }
        else if (error) {
            this.error = error;
            this.data = undefined;
        }
    }
    //this method is called when you clicked on the previous button
    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    //this method is called when you clicked on the next button
    nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    handleFirst() {  
        this.page = 1;  
        this.displayRecordPerPage(this.page);
      }  

      handleLast() {  
        this.page = this.totalPage;  
        this.displayRecordPerPage(this.page);
      }  
      handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.data));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.data = parseData;

    }
    //this method displays records page by page
    displayRecordPerPage(page){
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);
        this.endingRecord =
        (this.endingRecord > this.totalRecountCount) ?   this.totalRecountCount : this.endingRecord;
        this.data = this.items.slice(this.startingRecord,   this.endingRecord);
        this.startingRecord = this.startingRecord + 1;
    }
    handleContactName(event) {
       this.strSearchContactName = event.target.value;
       return refreshApex(this.result);
    }
    /* eslint-disable no-console */
    // eslint-disable-next-line no-console
    //this method holds the selected product.
    handleConfig(){
    var el = this.template.querySelector('lightning-datatable');
    var selected = el.getSelectedRows();
    }
    getSelectedRecords(event) {
        event.preventDefault();
        // getting selected rows
        const selectedRows = event.detail.selectedRows;
        window.console.log('selectedRows ====> ' +selectedRows);
        this.recordsCount = event.detail.selectedRows.length;
        // this set elements the duplicates if any
        window.console.log('recordsCount ====> ' +this.recordsCount);
        let conIds = new Set();
        var conids1='';
        // getting selected record id
        for (let i = 0; i < selectedRows.length; i++) {
            conIds.add(selectedRows[i].Id);
            conids1=selectedRows[i].Id;
            window.console.log('inside for loop ====> ' +conIds);
            window.console.log('conids1====> ' +conids1);
        }
        window.console.log('conIds ====> ' +conIds);
        window.console.log('conids2====> ' +conids1);
        // coverting to array
        //this.selectedRecords = Array.from(conIds);     
        window.console.log('selectedRecords ====> ' +this.selectedRecords);
        this.tasks = undefined;
        var str = conids1;
        console.log('str*******'+str);
        //var res = str.substring(0,15);
        var res = str;
        console.log('res*******'+res);
        var recId = res;
        console.log('recId*******'+recId);
        this.detailid=recId;
        console.log('detailid*******'+this.detailid);
        this.relobjname='Contact';
        this.cardtitle='Contact Details';
        this.selectedconId=recId;
        console.log('selectedconId*******'+this.selectedconId);
        //this.template.addEventListener('onshowtask', this.handleShowTask.bind(this));
        this.handleShowTask.bind(this);
        getOpportunitiesList({ conid: recId })
        .then(OpportunitiesList => {
        this.opportunities = OpportunitiesList;
        this.error1 = undefined;
        })
        .catch(error => {
        console.log('inside error');
        this.error1 = error;
        this.opportunities = 'No Records Found';
        });
        const name = recId;
        const selectEvent = new CustomEvent('showtask', {
            detail: name,bubbles: true
        });
        
        this.dispatchEvent(selectEvent);
        
        //return refreshApex(this.Opportunities);
    }
    constructor() {
        super();   
        //An OnShowTask Event Listener is added in the Super Constructor
        this.template.addEventListener('onshowtask', this.handleShowTask.bind(this));
    }
    //The HandleShowTask Function is called on click of opportunity to show related task
    handleShowTask(event){
        console.log('handle show task method called');
        console.log('selected opp id in handleshowtask'+event.detail);
        //this.selectedconId = event.detail;
    } 
//opportunity list method
opplistCallback(event1) {
    this.tasks = undefined;
    event1.preventDefault();
    const recId = event1.target.dataset.recordid;
    console.log('recId*******'+recId);
    this.detailid=recId;
    this.relobjname='Contact';
    this.cardtitle='Contact Details';
    getOpportunitiesList({ conid: recId })
    .then(OpportunitiesList => {
      this.opportunities = OpportunitiesList;
      this.error1 = undefined;
    })
    .catch(error => {
      this.error1 = error;
      this.opportunities = 'No Records Found';
    });
    return refreshApex(this.Opportunities);
}
  tasklistCallback(event2) {
    this.isLoaded = !this.isLoaded;
    event2.preventDefault();
    const recId = event2.target.dataset.recordid;
    console.log('oppid*******'+recId);
    //this.detailid=recId;
    //this.relobjname='Opportunity';
    //var parent = document.getElementsByClassName(recId)[0].parentNode;
    //parent.style.backgroundColor = 'red';
    getTasksList({ oppid: recId })
    .then(TaskList => {
      this.isLoaded = false;
      this.tasks = TaskList;
      this.error2 = undefined;
    })
    .catch(error => {
      this.error2 = error;
      this.tasks = undefined;
      this.isLoaded = false;
    });
    const event = new CustomEvent('recordsload', {
      detail: recordsCount
    });
    this.dispatchEvent(event);
    return refreshApex(this.Tasks);
  }
  taskdetailCallback(event3) {
    event3.preventDefault();
    const recId = event3.target.dataset.recordid;
    console.log('recId*******'+recId);
    this.detailid=recId;
    this.relobjname='Activity';
  }
  NewOpportunity() {
    console.log('inside new opportunity');
  }
}