import { LightningElement, track, wire ,api} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getcampaign from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.fetchCampaigns';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Hsmaquickfindercampaignlist extends LightningElement {
    @track data;
    @api selectedconId='';
    result;
    //this is initialize for 1st page
    @api page = 1;
    //it contains all the Product records.
    @track items = [];
    //To display the data into datatable
    @track data = [];
    //holds column info.
    @track columns;
    //start record position per page
    @track startingRecord = 1;
    //end record position per page
    @track endingRecord = 0;
    //10 records display per page
    @api pageSize = 5;
    @track setPagination;
    //total count of record received from all retrieved records
    @api totalRecountCount = 0;
    //total number of page is needed to display all records
    @track totalPage = 0;
    @track maxrowselection = 1;
   //To display the column into the data table
   @track columns = [
    {
     label: 'Name',
     fieldName: 'Name',
     type: 'text',
     sortable: "true"
     },
     {
        label: 'Status',
        fieldName: 'Status',
        type: 'text',
        sortable: "true"
    },
    {
        label: 'TYPE',
        fieldName: 'Type',
        type: 'text',
        sortable: "true"
    },
    {
        label: 'Reference URL',
        fieldName: 'SMAExternalReferenceURL__c',
        type: 'url',
        sortable: "true"
    },
    {
        label: 'Created BY',
        fieldName: 'CreatedBy.Name',
        type: 'text',
        sortable: "true"
    }, 
    {
        label: 'Created Date',
        fieldName: 'CreatedDate',
        type: 'date',
        sortable: "true"
    },   
    ];   
    //call the apex method and pass the search string into apex method.
    connectedCallback() {      
        console.log('Child Component selectedconId*******'); 
        //this.selectedconId='0038A00000R6sguQAB';
        //console.log('Child Component selectedconId*******'+this.selectedconId); 
    }
    wiredCampigns;
    @wire(getcampaign,{accId:'$selectedconId'})
    fetchOpps(result) {
        this.wiredCampigns = result;   
        if (result.data) {        
            this.items = result.data;
            this.setPagination = false;  
            console.log('result.data.length**********'+result.data.length); 
            this.totalRecountCount = result.data.length;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
            this.data = this.items.slice(0,this.pageSize);
            this.endingRecord = this.pageSize;
            this.error = undefined;  
            console.log('campign++'+this.items);
            if(this.totalRecountCount > this.pageSize){
                console.log('entered to set pagination');
                this.setPagination = true;
            }
            if(result.data.length==0){
                this.data = undefined;
            }
        } else if (result.error) {
            this.error = result.error;
            this.data = undefined; 
            this.totalRecountCount=0;           
        } 
        else{
            this.totalRecountCount=0;
            this.data = undefined; 
        }   
    }
    //this method is called when you clicked on the previous button
    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    //this method is called when you clicked on the next button
    nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    handleFirst() {  
        this.page = 1;  
        this.displayRecordPerPage(this.page);
    }  
    handleLast() {  
        this.page = this.totalPage;  
        this.displayRecordPerPage(this.page);
    }   
    handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.data));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.data = parseData;

    }
    handleConfig(){
        var el = this.template.querySelector('lightning-datatable');
        var selected = el.getSelectedRows();
        }  
    //this method displays records page by page
    displayRecordPerPage(page){
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);
        this.endingRecord =
        (this.endingRecord > this.totalRecountCount) ?   this.totalRecountCount : this.endingRecord;
        this.data = this.items.slice(this.startingRecord,   this.endingRecord);
        this.startingRecord = this.startingRecord + 1;
    }
    handleContactName(event) {
       this.strSearchContactName = event.target.value;
       return refreshApex(this.result);
    }
    /* eslint-disable no-console */
    // eslint-disable-next-line no-console
    //this method holds the selected product.
    handleConfig(){
    var el = this.template.querySelector('lightning-datatable');
    var selected = el.getSelectedRows();
    }
    /* private property is used to show/hide opp popup */
    @track bShowModal = false;
    /* JS function to open opp model  */ 
    openModal() {    
        // to open modal window set 'bShowModal' tarck value as true
        this.taskObject = {};
        console.log('open Modal check taskObject+++++'+JSON.stringify(this.taskObject));
        this.bShowModal = true;
    }
    /* JS function to close opp model */
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
    }
    @track newOppId;
    handleSuccess(event) {            
        /*console.log('check event'+JSON.stringify(event));*/
        console.log('inside handle sucess*************');
        const evt = new ShowToastEvent({
           title: 'Success!',
           message: "Campaign Member Successfully Added.",
           variant: 'success'
       });
       this.refreshData();
       this.dispatchEvent(evt);   
       this.bShowModal = false;
       return refreshApex(this.wiredCampigns);
             
    }
    refreshData() {
        console.log('inside refresh data*********');
        return refreshApex(this.wiredCampigns);
    }
    handleError(event) {
        const event1 = new ShowToastEvent({
            variant: 'error',
            message: 'Error ocurring while adding contact to campaign, Please reach system admin.',
        });
        this.refreshData();
        //this.bShowModal = false;
        this.dispatchEvent(event1);
        console.log('inside handle error*************');
        return refreshApex(this.wiredCampigns);
    }
}