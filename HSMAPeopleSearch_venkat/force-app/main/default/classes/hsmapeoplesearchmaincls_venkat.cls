public with sharing class hsmapeoplesearchmaincls_venkat {
    public hsmapeoplesearchmaincls_venkat() {

    }
    //below method will be used in mainComponent 
    @AuraEnabled(Cacheable = true)
    public static List<contact> retriveContacts(String strContactName) 
    {
    List<contact> lstcont=new List<contact>();
    String query = 'SELECT  Id, Name, Email,Phone FROM contact';
    if(strContactName != null && strContactName != '') 
    {      
        query = 'FIND \'' + strContactName + '\' IN ALL FIELDS RETURNING  contact (id,accountid,Email,Name,Phone)';
        List<List <sObject>> searchList = search.query(query);
        lstcont = ((List<contact>)searchList[0]);  
    }
    else {
        lstcont = Database.query(query);  
    }   
    return lstcont;
    }
    @AuraEnabled(cacheable = true) 
    public static List<Opportunity> getOpportunitiesList(string conid) {       
      list<opportunity> OpportunityList = new list<opportunity> ();      
      OpportunityList = [select id, name,StageName,Type,CloseDate from Opportunity where contactid=:conid];
      system.debug('>>>>>>>>>>>>>>> conAccOppsList : ' + OpportunityList.size() );
      if(OpportunityList.size() == 0){
          throw new AuraHandledException('No Record Found'); 
      }
      return OpportunityList;
     }
     @AuraEnabled(cacheable = true) 
     public static List<contact> getConlist(string conid) {       
      list<contact> conlist = new list<contact> ();      
      conlist = [select id, name,email,phone from contact where id=:conid];
      system.debug('>>>>>>>>>>>>>>> conAccOppsList : ' + conlist.size() );
      if(conlist.size() == 0){
          throw new AuraHandledException('No Details Found'); 
      }
      return conlist;
     }
     @AuraEnabled(cacheable = true) 
    public static List<Task> getTasksList(string oppid) {       
      list<task> TaskList = new list<task> ();      
      TaskList = [select id, subject,Priority,Status,Type,OwnerId,Owner.Name,ActivityDate from task where whatid=:oppid];
      system.debug('>>>>>>>>>>>>>>> TaskList : ' + TaskList.size() );
      if(TaskList.size() == 0){
          throw new AuraHandledException('No Record Found'); 
      }
      return TaskList;
     } 
      @AuraEnabled(cacheable = true) 
      public static List<Task> getTasksList(Integer pagenumber, Integer numberOfRecords, Integer pageSize, String searchString) {  
        String searchKey = '%' + searchString + '%';  
        String query = 'select id, Name,Priority from Task ';  
         
        return Database.query(query);  
      }  
    @AuraEnabled(cacheable = true)  
    public static Integer getAccountsCount(String searchString) {  
      String query = 'select count() from Account ';  
      system.debug('query****'+query);
      return Database.countQuery(query);  
    } 
    @AuraEnabled(cacheable = true)  
    public static Integer getContactsCount(String searchString) {  
      list<contact> ContactsSearchList = new list<contact>();
      Integer concount=0;
      String searchStr = '*'+searchString+'*';
      String searchQuery = 'FIND \'' + searchStr + '\' IN ALL FIELDS RETURNING  contact (id,accountid,Email,Name,Phone)';
      List<List <sObject>> searchList = search.query(searchQuery);
      ContactsSearchList = ((List<contact>)searchList[0]);

      system.debug('Contact size: ' + ContactsSearchList.size() );


      if(ContactsSearchList.size() == 0){
          throw new AuraHandledException('No Record Found'); 
      }
      else {
        {
          concount=ContactsSearchList.size();
        }
      }
      return concount;
    }  
}
