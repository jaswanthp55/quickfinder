import { LightningElement, track, wire ,api} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import serachCons from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.retriveContacts';
import getOpportunitiesList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getOpportunitiesList';
import getTasksList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getTasksList';
import NAME_FIELD from '@salesforce/schema/contact.Name';
import TITLE_FIELD from '@salesforce/schema/contact.Title';
import PHONE_FIELD from '@salesforce/schema/contact.Phone';
import MOBILE_FIELD from '@salesforce/schema/contact.Phone';
import EMAIL_FIELD from '@salesforce/schema/contact.Email';
import CONTACTTYPE_FIELD from '@salesforce/schema/contact.zipsma__ContactType__c';
import MARKETSEGMENT_FIELD from '@salesforce/schema/contact.zipsma__MarketSegment__c';
import OWNER_FIELD from '@salesforce/schema/contact.OwnerId';
export default class Hsmapeoplesearchmainvenkat extends LightningElement {
    @track data;
    @track strSearchContactName = '';
    result;
//this is initialize for 1st page
    @track page = 1;
//it contains all the Product records.
    @track items = [];
//To display the data into datatable
    @track data = [];
//holds column info.
    @track columns;
//start record position per page
    @track startingRecord = 1;
//end record position per page
    @track endingRecord = 0;
//10 records display per page
    @track pageSize = 5;
//total count of record received from all retrieved records
    @track totalRecountCount = 0;
//total number of page is needed to display all records
    @track totalPage = 0;
    @track maxrowselection = 1;
//To display the column into the data table
   @track accounts;
   @track contacts;
   @track contacts1;
   @track opportunities;
   @track opportunities1;
   @track tasks;
   @track error;
   @track error1;
   @track error2;
   @track searchKey;
   @track detailid;
   @track cardtitle;
   @track relobjname;
   @track RecDetailFields = [OWNER_FIELD,NAME_FIELD, TITLE_FIELD, PHONE_FIELD, MOBILE_FIELD, EMAIL_FIELD,CONTACTTYPE_FIELD,MARKETSEGMENT_FIELD];
    @track columns = [
    {
     label: 'Name',
     fieldName: 'Name',
     type: 'text',
     },
    {
        label: 'Email',
        fieldName: 'Email',
        type: 'email',
    },
    {
        label: 'Phone',
        fieldName: 'Phone',
        type: 'phone',
    },
    ];
    //call the apex method and pass the search string into apex method.
    @wire(serachCons, {strContactName : '$strSearchContactName' })
    wiredProducts({ error, data }) {
        if (data) {
            this.items = data;
            this.totalRecountCount = data.length;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
            this.data = this.items.slice(0,this.pageSize);
            this.endingRecord = this.pageSize;
            this.error = undefined;
        }
        else if (error) {
            this.error = error;
            this.data = undefined;
        }
    }
    //this method is called when you clicked on the previous button
    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    //this method is called when you clicked on the next button
    nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    //this method displays records page by page
    displayRecordPerPage(page){
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);
        this.endingRecord =
        (this.endingRecord > this.totalRecountCount) ?   this.totalRecountCount : this.endingRecord;
        this.data = this.items.slice(this.startingRecord,   this.endingRecord);
        this.startingRecord = this.startingRecord + 1;
    }
    handleContactName(event) {
       this.strSearchContactName = event.target.value;
       return refreshApex(this.result);
    }
    /* eslint-disable no-console */
    // eslint-disable-next-line no-console
    //this method holds the selected product.
    handleConfig(){
    var el = this.template.querySelector('lightning-datatable');
    var selected = el.getSelectedRows();
    }
    getSelectedRecords(event) {
       
        // getting selected rows
        const selectedRows = event.detail.selectedRows;
        window.console.log('selectedRows ====> ' +selectedRows);
        this.recordsCount = event.detail.selectedRows.length;
        // this set elements the duplicates if any
        window.console.log('recordsCount ====> ' +this.recordsCount);
        let conIds = new Set();
        var conids1='';
        // getting selected record id
        for (let i = 0; i < selectedRows.length; i++) {
            conIds.add(selectedRows[i].Id);
            conids1=selectedRows[i].Id;
            window.console.log('inside for loop ====> ' +conIds);
            window.console.log('conids1====> ' +conids1);
        }
        window.console.log('conIds ====> ' +conIds);
        window.console.log('conids2====> ' +conids1);
        // coverting to array
        //this.selectedRecords = Array.from(conIds);     
        window.console.log('selectedRecords ====> ' +this.selectedRecords);
        this.tasks = undefined;
        var str = conids1;
        console.log('str*******'+str);
        //var res = str.substring(0,15);
        var res = str;
        console.log('res*******'+res);
        var recId = res;
        console.log('recId*******'+recId);
        this.detailid=recId;
        console.log('detailid*******'+this.detailid);
        this.relobjname='Contact';
        this.cardtitle='Contact Details';
        getOpportunitiesList({ conid: recId })
        .then(OpportunitiesList => {
        this.opportunities = OpportunitiesList;
        this.error1 = undefined;
        })
        .catch(error => {
        console.log('inside error');
        this.error1 = error;
        this.opportunities = 'No Records Found';
        });
        return refreshApex(this.Opportunities);
    }
//opportunity list method
opplistCallback(event1) {
    this.tasks = undefined;
    event1.preventDefault();
    const recId = event1.target.dataset.recordid;
    console.log('recId*******'+recId);
    this.detailid=recId;
    this.relobjname='Contact';
    this.cardtitle='Contact Details';
    getOpportunitiesList({ conid: recId })
    .then(OpportunitiesList => {
      this.opportunities = OpportunitiesList;
      this.error1 = undefined;
    })
    .catch(error => {
      this.error1 = error;
      this.opportunities = 'No Records Found';
    });
    return refreshApex(this.Opportunities);
}
  tasklistCallback(event2) {
    this.isLoaded = !this.isLoaded;
    event2.preventDefault();
    const recId = event2.target.dataset.recordid;
    console.log('recId*******'+recId);
    //this.detailid=recId;
    //this.relobjname='Opportunity';
    getTasksList({ oppid: recId })
    .then(TaskList => {
      this.isLoaded = false;
      this.tasks = TaskList;
      this.error2 = undefined;
    })
    .catch(error => {
      this.error2 = error;
      this.tasks = undefined;
      this.isLoaded = false;
    });
    const event = new CustomEvent('recordsload', {
      detail: recordsCount
    });
    this.dispatchEvent(event);
    return refreshApex(this.Tasks);
  }
  taskdetailCallback(event3) {
    event3.preventDefault();
    const recId = event3.target.dataset.recordid;
    console.log('recId*******'+recId);
    this.detailid=recId;
    this.relobjname='Activity';
  }
  NewOpportunity() {
    console.log('inside new opportunity');
  }
}